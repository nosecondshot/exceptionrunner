https://bitbucket.org/nosecondshot/exceptionrunner/src/master

Tasks to answer in your own README.md that you submit on Canvas:

1. See logger.log, why is it different from the log to console?

   logger.log contains the fail test, instead of putting all problems to the screen, it will output the warning message to the file.
   This is because it used the try catch block to get the problem to put the message to the file.

2. Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

   This come from when the first test done, it shows that FINER log which means this works well. Finer level.

3. What does Assertions.assertThrows do?

   We expect it will throw an exception when an exception appears.

4. See TimerException and there are 3 questions
    1. What is serialVersionUID and why do we need it? (please read on Internet)

       The serialVersionUID attribute is an identifier that is used to serialize/deserialize an object of a Serializable class. We use that to verify that a loaded class and the serialized object are compatible.
    2. Why do we need to override constructors?

       We want a new message to be pass in and don't want it to have a conflict with the original oneAlso, we want to define a behavior that's specific to the subclass type.
    3. Why we did not override other Exception methods?

       We do not use those.


5. The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

   This is to static initialize of a class, the code inside only execute once. This static block is to get the config file for Timer, it will print "starting the app" when it runs successful.

6. What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

   This is a Markdown File format, Bitbucket uses CodeMirror to apply syntax highlighting to the rendered markdown in comments, READMEs and pull request descriptions.

7. Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

   Because in Timer, method TimeMe when the input is a number less than zero, "logger.info("Calling took: "+ (System.currentTimeMillis() - timeNow));" this line can't be executed, so I add a if block to check if the input is larger than 0

8. What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

   The exception can be caught normally, but it will go to final block, so that part can't run.

9. Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel)

![](Screen Shot 2022-04-06 at 11.08.55 PM.png)

10. Make a printScreen of your eclipse Maven test run, with console

![](Screen Shot 2022-04-06 at 11.08.55 PM.png)

11. What category of Exceptions is TimerException and what is NullPointerException

    TimerException is runtime exception, it will be caught when the program executes. NullPointerException is caught when a variable is accessed which is not pointing to any object and refers to nothing or null.

12. Push the updated/fixed source code to your own repository.